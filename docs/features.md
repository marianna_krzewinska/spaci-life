# Features

## Spacers

---

Reminders defined in Spaci are called Spacers. The Spacer contains the activity's general configuration: its name, how often Spaci should remind you about it, and with which sound.

---

### After clicking or adding a Spacer, you'll be redirected to Spacer edit view

</br>

<img src="./screenshots/main-view.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/spacer-view.jpg" height=500>

---

### There you can change name, interval, sound and delete the Spacer. Changes are automatically saved

</br>

<img src="./screenshots/name-change.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/interval-change.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/sound-change.jpg" height=500>

---

### The information about next Spacers will also appear as a persistent notification

</br>

<img src="./screenshots/notification.jpg" height=300>

---

## Silent Time

---

### From the main screen, you can go to settings by clicking gearwheel in the upper right corner.

</br>

<img src="./screenshots/main-view.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/settings-inactive.jpg" height=500>

---

### There you can configure Silent Time. During this time Spacers won't play.

</br>

<img src="./screenshots/settings-inactive.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/settings-active.jpg" height=500>

---

### After turning Silent Time on, you'll be able to change the time

</br>

<img src="./screenshots/set-time-clock.jpg" height=500> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="./screenshots/set-time-manual.jpg" height=500>
