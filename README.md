# Spaci

[![coverage report](https://gitlab.com/marianna_krzewinska/spaci-life/badges/master/coverage.svg)](https://gitlab.com/marianna_krzewinska/spaci-life/-/commits/master)

## About

Changes rarely happen overnight or after singular activity. An easier way to significantly change our life is through small, repeatable activities. But keeping up with these activities requires a lot of mental space or the use of multiple dedicated tools. It all can eat up our ability to focus on the tasks at hand.

Spaci is a response to the need for a simple and more minimalistic way to implement a day-to-day plan. Spaci achieves it by reminding you unobtrusively about your planned activities, allowing you to be more focused and productive.

**Project status: MVP**

**Supported platforms: Android v6+**

## Usage

</br>

<img src="docs/screenshots/screen-record.gif" height=500>

</br>

### For more usage tips, check out [Features](./docs/features.md).

## Installation

### APK files for each version are available in [Releases](https://gitlab.com/marianna_krzewinska/spaci-life/-/releases).


## Development

### Prerequisites

1. Operating system - currently, the only supported OS is **Linux** - but configurations for Windows & macOS [are coming](https://gitlab.com/marianna_krzewinska/spaci-life/-/issues/19)

2. Mobile device - current development setup requires a device with **Android v6 or higher** to be connected by USB. The project uses Android-specific libraries, so iOS and other operating systems are not supported. We're planning to add support for the Android Studio emulator [soon #18](https://gitlab.com/marianna_krzewinska/spaci-life/-/issues/18)

<!-- TODO: check if other settings on the device are required -->

### How to build

1. Clone the repository
2. Run `docker-compose up` in the main folder - It will build a docker image with Flutter and Code Server, after which it'll start a Code Server instance with application files at http://localhost:8080
   - [How to change Code Server port](docs/FAQ.md#how-to-change-code-server-port)
3. In the Code Server start `Run and Debug` process - it will launch the app on your device in debug mode (see [launch config](.vscode/launch.json) for more details)

## Built with

- [VS Code as Code Server](https://github.com/cdr/code-server) - IDE
- [Flutter](https://github.com/flutter/flutter) - framework
- [Dart](https://dart.dev/) - programming language
- [Redux using flutter_redux](https://redux.js.org/tutorials/fundamentals/part-1-overview) - app state management
- [SQLite](https://flutter.dev/docs/cookbook/persistence/sqlite) - persistent data storage

## Authors

- [Marianna Krzewińska](https://gitlab.com/marianna_krzewinska)

## Acknownledgments

- Dockerized setup inspired by [Souvik Biswas's blog post](https://blog.codemagic.io/how-to-dockerize-flutter-apps/)
