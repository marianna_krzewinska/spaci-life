import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_epics/redux_epics.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/main_database.dart';
import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/spacers/spacer_view.dart';
import 'package:spaci/spacers/spacers_view.dart';


final List<AppSpacer> initialSpacersState = List.unmodifiable([]);

List<Function> spacersEpics = [
  getSpacersEpic,
  createSpacerEpic,
  updateSpacerEpic,
  deleteSpacerEpic,
];

List<AppSpacer> spacersReducer(AppState state, SpacerAction action) {
  switch (action.type) {
    case SpacerActions.CreateSpacerSuccess:
      return [...state.spacers, action.spacer];
    case SpacerActions.UpdateSpacerSuccess:
      return [...state.spacers.map((spacer) => spacer.id == action.spacer.id
        ? action.spacer
        : spacer)
      ];
    case SpacerActions.DeleteSpacerSuccess:
      return [...state.spacers.where((spacer) => spacer.id != action.id)];
    case SpacerActions.GetSpacersSuccess:
      return action.spacers;
    default:
      break;
  }

  return state.spacers;
}

Stream<dynamic> getSpacersEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SpacerActions.GetSpacers)
    .asyncMap((action) async {
      final List<AppSpacer> spacers = await database.getSpacers();

      return GetSpacersSuccessAction(spacers);
    });
}

Stream<dynamic> createSpacerEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SpacerActions.CreateSpacer)
    .asyncMap((action) async {
      final AppSpacer spacer = await database.addSpacer(action.spacer);

      return [
        CreateSpacerSuccessAction(spacer),
        NavigateToAction.push(
          SpacerView.route,
          arguments: SpacerScreenArgs(spacer.id)
        ),
      ];
    })
    .expand((actions) => actions);
}

Stream<dynamic> updateSpacerEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SpacerActions.UpdateSpacer)
    .asyncMap((action) async {
      final AppSpacer spacer = await database.updateSpacer(
        id: action.id,
        name: action.name ?? null,
        intervalKey: action.intervalKey ?? null,
        sound: action.sound ?? null,
      );

      return UpdateSpacerSuccessAction(spacer);
    });
}

Stream<dynamic> deleteSpacerEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SpacerActions.DeleteSpacer)
    .asyncMap((action) async {
      await database.deleteSpacer(action.id);

      return [
        DeleteSpacerSuccessAction(action.id),
        NavigateToAction.pushNamedAndRemoveUntil(SpacersView.route, (_) => false),
      ];
    })
    .expand((actions) => actions);
}

abstract class SpacerAction {
  SpacerActions type;

  AppSpacer spacer;
  List<AppSpacer> spacers;
  String name;
  String intervalKey;
  String sound;
  int id;
}

class UpdateSpacerAction extends SpacerAction {
  final SpacerActions type = SpacerActions.UpdateSpacer;

  int id;
  String name;
  String sound;
  String intervalKey;

  UpdateSpacerAction(this.id, { this.name, this.sound, this.intervalKey });
}

class UpdateSpacerSuccessAction extends SpacerAction {
  final SpacerActions type = SpacerActions.UpdateSpacerSuccess;
  AppSpacer spacer;

  UpdateSpacerSuccessAction(this.spacer);
}

class CreateSpacerAction extends SpacerAction {
  final SpacerActions type = SpacerActions.CreateSpacer;
  AppSpacer spacer;

  CreateSpacerAction(this.spacer);
}

class CreateSpacerSuccessAction extends SpacerAction {
  final SpacerActions type = SpacerActions.CreateSpacerSuccess;
  AppSpacer spacer;

  CreateSpacerSuccessAction(this.spacer);
}

class DeleteSpacerAction extends SpacerAction {
  final SpacerActions type = SpacerActions.DeleteSpacer;
  int id;

  DeleteSpacerAction(this.id);
}

class DeleteSpacerSuccessAction extends SpacerAction {
  final SpacerActions type = SpacerActions.DeleteSpacerSuccess;
  int id;

  DeleteSpacerSuccessAction(this.id);
}

class GetSpacersAction extends SpacerAction {
  final SpacerActions type = SpacerActions.GetSpacers;
}

class GetSpacersSuccessAction extends SpacerAction {
  final SpacerActions type = SpacerActions.GetSpacersSuccess;
  List<AppSpacer> spacers;

  GetSpacersSuccessAction(this.spacers);
}

enum SpacerActions {
  Empty,
  UpdateSpacer,
  UpdateSpacerSuccess,
  CreateSpacer,
  CreateSpacerSuccess,
  DeleteSpacer,
  DeleteSpacerSuccess,
  GetSpacers,
  GetSpacersSuccess,
}
