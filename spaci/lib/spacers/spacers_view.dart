import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/logs.dart';
import 'package:spaci/settings/settings_view.dart';
import 'package:spaci/settings/settings_state.dart';

import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/spacers/spacers_state.dart';
import 'package:spaci/spacers/spacer_view.dart';

class SpacersView extends StatelessWidget {
  static const route = '/spacers';

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);
    store.dispatch(GetSpacersAction());
    store.dispatch(GetSettingsAction());

    return Scaffold(
      appBar: AppBar(
        title: StoreConnector<AppState, String>(
          builder: (_, title) => Text(title),
          converter: (store) => '${store.state.title}${store.state.spacers.length.toString()}',
        ),

        actions: [
          StoreConnector<AppState, bool>(
            builder: (_, isDisabled) => Switch(
              value: !isDisabled,
              onChanged: (_) => store.dispatch(ToggleSettingsDisabledAction()),
            ),
            converter: (store) => store.state.settings.isDisabled,
          ),

          if (kDebugMode) IconButton(
            icon: Icon(Icons.comment),
            onPressed: () => store.dispatch(NavigateToAction
              .push(LogsView.route))),

          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => store.dispatch(NavigateToAction
              .push(SettingsView.route))),
        ],
      ),

      body: StoreConnector<AppState, List<AppSpacer>>(
        builder: (context, spacers) => ListView(
          children: spacers
            .map((spacer) => ListTile(
              title: Text('${spacer.name}, interval: ${intervals[spacer.intervalKey].name}'),
              onTap: () => store.dispatch(NavigateToAction
                .push(SpacerView.route, arguments: SpacerScreenArgs(spacer.id))),
            ))
            .toList(),
        ),
        converter: (store) => store.state.spacers,
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => store.dispatch(CreateSpacerAction(AppSpacer.create())),
        tooltip: 'Add Spacer',
        child: Icon(Icons.add),
      ),
    );
  }
}
