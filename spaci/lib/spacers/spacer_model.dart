NotificationSound _defaultSound = sounds['single'];
SpacerInterval _defaultInterval = intervals['Min15'];

class AppSpacer {
  String name;
  int id;
  String intervalKey;
  String sound;

  AppSpacer(int id, String name, String intervalKey, String sound) {
    this.id = id;
    this.name = name;
    this.intervalKey = intervals.containsKey(intervalKey)
      ? intervalKey
      : _defaultInterval.key;
    this.sound = sounds.containsKey(sound) ? sound : _defaultSound.key;
  }

  static AppSpacer create() {
    return AppSpacer(
      DateTime.now().millisecondsSinceEpoch,
      '',
      _defaultInterval.key,
      _defaultSound.key,
    );
  }

  static AppSpacer clone(AppSpacer spacer) {
    return AppSpacer(spacer.id, spacer.name, spacer.intervalKey, spacer.sound);
  }

  AppSpacer update({ newName, newIntervalKey, newSound }) {
    return AppSpacer(
      id,
      newName ?? name,
      newIntervalKey ?? intervalKey,
      newSound ?? sound,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'id': id,
      'intervalKey': intervalKey,
      'sound': sound,
    };
  }
}

class NotificationSound {
  final String name;
  final String key;
  final double volume;

  NotificationSound(this.name, this.key, this.volume);
}

final Map<String, NotificationSound> sounds = {
  'single': NotificationSound('Single', 'single', 1),
  'double': NotificationSound('Double', 'double', 1),
  'definite-555': NotificationSound('Definite1', 'definite-555', 0.5),
  'juntos-607': NotificationSound('Juntos', 'juntos-607', 0.5),
  'just-saying-593': NotificationSound('Just Saying', 'just-saying-593', 0.5),
  'point-blank-589': NotificationSound('Point Blank', 'point-blank-589', 0.5),
  'pristine-609': NotificationSound('Pristine', 'pristine-609', 0.5),
  'sharp-592': NotificationSound('Sharp', 'sharp-592', 0.5),
  'to-the-point-568': NotificationSound('To the Point', 'to-the-point-568', 0.5),
};

class SpacerInterval {
  final String key;
  final String name;
  final int intervalInMinutes;

  SpacerInterval(this.key, this.name, this.intervalInMinutes);
}

final Map<String, SpacerInterval> intervals = {
  'Min15': SpacerInterval('Min15', '15 min', 15),
  'Min30': SpacerInterval('Min30', '30 min', 30),
  'Min60': SpacerInterval('Min60', '1 h', 60),
  'Min120': SpacerInterval('Min120', '2 h', 120),
  'Min180': SpacerInterval('Min180', '3 h', 180),
  'Min240': SpacerInterval('Min240', '4 h', 240),
};
