import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/spacers/spacers_state.dart';
import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/foreground_service.dart';


class SpacerScreenArgs {
  final int id;

  SpacerScreenArgs(this.id);
}

class SpacerView extends StatefulWidget {
  static const route = '/spacer';

  @override
  State<StatefulWidget> createState() => _SpacerViewState();
}

class _SpacerViewState extends State<SpacerView> {
  final _formKey = GlobalKey<FormState>();
  String _sound;

  @override
  Widget build(BuildContext context) {

    final SpacerScreenArgs args = ModalRoute.of(context).settings.arguments;

    final store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, AppSpacer>(
      distinct: true,
      converter: (store) => AppSpacer.clone(store.state.spacers
        .singleWhere((element) => element.id == args.id, orElse: AppSpacer.create)),
      builder: (context, spacer) {

        return Scaffold(
          appBar: AppBar(),

          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 31),
                        child: Text(
                          'Name: ',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      Container(
                        width: 200,
                        child: TextFormField(
                          initialValue: spacer.name,
                          onChanged: (value) => store
                            .dispatch(UpdateSpacerAction(spacer.id, name: value)),
                        ),
                      ),
                    ]),

                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: Text(
                          'Interval: ',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        )),

                      DropdownButton<String>(
                        value: spacer.intervalKey,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        onChanged: (String newValue) {
                          store.dispatch(UpdateSpacerAction(
                            spacer.id,
                            intervalKey: newValue,
                          ));
                        },
                        items: intervals.values
                          .map<DropdownMenuItem<String>>((SpacerInterval value) {
                            return DropdownMenuItem<String>(
                              value: value.key,
                              child: Text(value.name),
                            );
                          })
                          .toList(),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 28),
                        child: Text(
                          'Sound: ',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        )),

                      DropdownButton<String>(
                        value: spacer.sound,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        onChanged: (String newValue) {
                          store.dispatch(UpdateSpacerAction(
                            spacer.id,
                            sound: newValue,
                          ));

                          setState(() {
                            _sound = newValue;
                          });
                        },
                        items: sounds.values
                          .map<DropdownMenuItem<String>>((NotificationSound value) {
                            return DropdownMenuItem<String>(
                              value: value.key,
                              child: Text(value.name),
                            );
                          })
                          .toList(),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 30),
                        child: IconButton(
                          iconSize: 30,
                          icon: Icon(Icons.play_arrow),
                          onPressed: () => playSound(_sound ?? spacer.sound),
                        ),
                      ),
                    ],
                  ),

                  StoreConnector<AppState, dynamic>(
                    converter: (store) => (id) => store.dispatch(DeleteSpacerAction(id)),
                    builder: (_, _delete) => GestureDetector(
                      onTap: () => _delete(spacer.id),
                      child:  Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 0, top: 5),
                            child: IconButton(
                            icon: Icon(Icons.delete, size: 35, color: Colors.red[900]),
                            onPressed: null)),

                          Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              'Delete',
                              style: TextStyle(
                                color: Colors.red[900],
                                fontSize: 20,
                                height: 1.5,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    );
  }
}
