import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/main_database.dart';

import 'package:spaci/foreground_service.dart';
import 'package:spaci/logs.dart';

import 'package:spaci/spacers/spacers_view.dart';
import 'package:spaci/spacers/spacer_view.dart';
import 'package:spaci/settings/settings_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await database.init();

  final epicMiddleware = EpicMiddleware<AppState>(epics);

  final store = Store<AppState>(
    reducer,
    initialState: AppState.initial(),
    middleware: [
      epicMiddleware,
      NavigationMiddleware<AppState>(),
    ],
  );

  runApp(App(
    store: store,
  ));

  await updateForegroundService();
  await scheduleLogsReset();
}

class App extends StatelessWidget {

  final Store<AppState> store;

  App({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Spaci',
        theme: ThemeData.dark(),
        initialRoute: SpacersView.route,
        routes: {
          SpacersView.route: (context) => SpacersView(),
          SpacerView.route: (context) => SpacerView(),
          SettingsView.route: (context) => SettingsView(),
          LogsView.route: (context) => LogsView(),
        },
        navigatorKey: NavigatorHolder.navigatorKey,
      ),
    );
  }
}
