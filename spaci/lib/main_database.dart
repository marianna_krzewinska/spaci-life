import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:spaci/spacers/spacer_model.dart';

DB database = DB();

class DB {
  Database _database;

  Future<Database> init() async {
    _database = await openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'spacers_database.db'),

      onUpgrade: _runMigrations,
      onDowngrade: _revertMigrations,
      version: 3,
    );

    return _database;
  }

  Future<Database> getDB() async {
    return _database ?? await init();
  }

  Future<AppSpacer> addSpacer(AppSpacer spacer) async {
    final Database db = await getDB();

    await db.insert(
      'spacers',
      spacer.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);

    return spacer;
  }

  Future<List<AppSpacer>> getSpacers() async {
    final Database db = await getDB();

    List<Map> spacers = await db
      .rawQuery('SELECT id, name, intervalKey, sound FROM spacers');

    return spacers
      .map((Map spacer) => AppSpacer(
        spacer['id'],
        spacer['name'],
        spacer['intervalKey'],
        spacer['sound'],
      ))
      .toList();
  }

  Future<AppSpacer> updateSpacer({ String name, String intervalKey, String sound, int id }) async {
    final Database db = await getDB();

    await db.update(
      'spacers',
      {
        if (name != null) ...{ 'name': name },
        if (intervalKey != null) ...{ 'intervalKey': intervalKey },
        if (sound != null) ...{ 'sound': sound },
      },
      where: 'id = ?',
      whereArgs: [id],
    );

    return (await db
      .query(
        'spacers',
        where: 'id = ?',
        whereArgs: [id],
        columns: ['name', 'intervalKey', 'sound', 'id']))
      .map((row) => AppSpacer(
        row['id'],
        row['name'],
        row['intervalKey'],
        row['sound'],
      ))
      .first;
  }

  Future<int> deleteSpacer(int id) async {
    final Database db = await getDB();

    return await db.delete('spacers', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> getSpacersCount() async {
    final Database db = await getDB();

    return Sqflite
      .firstIntValue(await db.rawQuery('SELECT COUNT(*) from spacers'));
  }

}

final Map<int, Map<String, Function>> _migrations = {
  1: {
    'up': (db) async => db.execute('CREATE TABLE spacers(id INTEGER PRIMARY KEY, name TEXT)'),
  },
  2: {
    'up': (db) async => db.execute('ALTER TABLE spacers ADD COLUMN intervalKey TEXT'),
  },
  3: {
    'up': (db) async => db.execute('ALTER TABLE spacers ADD COLUMN sound TEXT'),
  }
};

Future<void> _runMigrations(db, oldVersion, newVersion) async {
  for (var i = oldVersion + 1; i <= newVersion; i++) {
    if (_migrations.containsKey(i) && _migrations[i]['up'] is Function) {
      await _migrations[i]['up'](db);
    }
  }
}

Future<void> _revertMigrations(db, oldVersion, newVersion) async {
  for (var i = oldVersion; i > newVersion; i--) {
    if (_migrations.containsKey(i) && _migrations[i]['down'] is Function) {
      await _migrations[i]['down'](db);
    }
  }
}
