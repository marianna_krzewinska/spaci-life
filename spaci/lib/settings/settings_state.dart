import 'package:flutter/material.dart';
import 'package:redux_epics/redux_epics.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/settings/settings_model.dart';

final Settings settingsInitialState = Settings();

final List<Function> settingsEpics = [
  getSettingsEpic,
  setSettingsDoNotDisturbEpic,
  setSettingsDoNotDisturbStartEpic,
  setSettingsDoNotDisturbEndEpic,
  toggleSettingsDisabledEpic,
];

enum SettingsActions {
  GetSettings,
  GetSettingsSuccess,
  SetSettingsDoNotDisturb,
  SetSettingsDoNotDisturbStart,
  SetSettingsDoNotDisturbEnd,
  ToggleSettingsDisabled,
}

abstract class SettingsAction {
  SettingsActions type;

  bool doNotDisturb;
  Settings settings;
  TimeOfDay doNotDisturbStart;
  TimeOfDay doNotDisturbEnd;
}

class GetSettingsAction extends SettingsAction {
  final SettingsActions type = SettingsActions.GetSettings;
}

class GetSettingsSuccessAction extends SettingsAction {
  Settings settings;
  final SettingsActions type = SettingsActions.GetSettingsSuccess;

  GetSettingsSuccessAction(this.settings);
}

class SetSettingsDoNotDisturbAction extends SettingsAction {
  bool doNotDisturb;
  final SettingsActions type = SettingsActions.SetSettingsDoNotDisturb;

  SetSettingsDoNotDisturbAction(this.doNotDisturb);
}

class SetSettingsDoNotDisturbStartAction extends SettingsAction {
  TimeOfDay doNotDisturbStart;
  final SettingsActions type = SettingsActions.SetSettingsDoNotDisturbStart;

  SetSettingsDoNotDisturbStartAction(this.doNotDisturbStart);
}

class SetSettingsDoNotDisturbEndAction extends SettingsAction {
  TimeOfDay doNotDisturbEnd;
  final SettingsActions type = SettingsActions.SetSettingsDoNotDisturbEnd;

  SetSettingsDoNotDisturbEndAction(this.doNotDisturbEnd);
}

class ToggleSettingsDisabledAction extends SettingsAction {
  final SettingsActions type = SettingsActions.ToggleSettingsDisabled;

  ToggleSettingsDisabledAction();
}

Settings settingsReducer (AppState state, SettingsAction action) {
  switch (action.type) {
    case SettingsActions.GetSettingsSuccess:
      return action.settings;
    default:
      break;
  }

  return state.settings;
}

Stream<dynamic> getSettingsEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SettingsActions.GetSettings)
    .asyncMap((action) async {
      Settings settings = await Settings.getFromStore();

      return GetSettingsSuccessAction(settings);
    });
}

Stream<dynamic> setSettingsDoNotDisturbEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SettingsActions.SetSettingsDoNotDisturb)
    .asyncMap((action) async {
      await Settings.updateDoNotDisturb(action.doNotDisturb);

      return GetSettingsAction();
    });
}

Stream<dynamic> setSettingsDoNotDisturbStartEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SettingsActions.SetSettingsDoNotDisturbStart)
    .asyncMap((action) async {
      await Settings.updateDoNotDisturbStart(action.doNotDisturbStart);

      return GetSettingsAction();
    });
}

Stream<dynamic> setSettingsDoNotDisturbEndEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SettingsActions.SetSettingsDoNotDisturbEnd)
    .asyncMap((action) async {
      await Settings.updateDoNotDisturbEnd(action.doNotDisturbEnd);

      return GetSettingsAction();
    });
}

Stream<dynamic> toggleSettingsDisabledEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => action.type == SettingsActions.ToggleSettingsDisabled)
    .asyncMap((action) async {
      await Settings.toggleDisabled();

      return GetSettingsAction();
    });
}
