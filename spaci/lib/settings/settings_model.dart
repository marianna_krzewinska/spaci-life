import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:spaci/utils.dart';

const TimeOfDay _defaultStartTime = TimeOfDay(hour: 22, minute: 0);
const TimeOfDay _defaultEndTime = TimeOfDay(hour: 10, minute: 0);
const bool _defaultIsDisabled = true;
const bool _defaultDoNotDisturb = false;

const String _doNotDisturbKey = 'doNotDisturb';
const String _doNotDisturbStartKey = 'doNotDisturbStart';
const String _doNotDisturbEndKey = 'doNotDisturbEnd';
const String _disabledKey = 'disabled';

const String _timeParseRegExp = r"^\d+:\d+";

class Settings {
  bool doNotDisturb;
  TimeOfDay doNotDisturbStart;
  TimeOfDay doNotDisturbEnd;
  bool isDisabled;

  Settings({
    this.doNotDisturb = _defaultDoNotDisturb,
    this.doNotDisturbStart = _defaultStartTime,
    this.doNotDisturbEnd = _defaultEndTime,
    this.isDisabled = _defaultIsDisabled,
  });

  static Settings clone(Settings settings) {
    return Settings(
      doNotDisturb: settings.doNotDisturb,
      doNotDisturbStart: settings.doNotDisturbStart,
      doNotDisturbEnd: settings.doNotDisturbEnd,
      isDisabled: settings.isDisabled,
    );
  }

  static TimeOfDay timeOfDayFromString(String timeOfDayString, TimeOfDay defaultTime) {
    bool isTime = RegExp(_timeParseRegExp).hasMatch(timeOfDayString ?? '');

    if (!isTime) { return defaultTime; }

    List<int> timeArray = timeOfDayString
      .split(':')
      .map((e) => int.parse(e))
      .toList();

    return TimeOfDay(hour: timeArray[0], minute: timeArray[1]);
  }

  static Future<Settings> getFromStore() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.reload();

    bool doNotDisturb = sharedPreferences.getBool(_doNotDisturbKey) ?? _defaultDoNotDisturb;
    TimeOfDay doNotDisturbStart = Settings.timeOfDayFromString(
      sharedPreferences.getString(_doNotDisturbStartKey),
      _defaultStartTime);
    TimeOfDay doNotDisturbEnd = Settings.timeOfDayFromString(
      sharedPreferences.getString(_doNotDisturbEndKey),
      _defaultEndTime);
    bool isDisabled = sharedPreferences.getBool(_disabledKey) ?? _defaultIsDisabled;

    return Settings(
      doNotDisturb: doNotDisturb,
      doNotDisturbStart: doNotDisturbStart,
      doNotDisturbEnd: doNotDisturbEnd,
      isDisabled: isDisabled,
    );
  }

  static Future<bool> updateDoNotDisturb(bool doNotDisturb) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return await sharedPreferences.setBool(_doNotDisturbKey, doNotDisturb);
  }

  static Future<bool> updateDoNotDisturbStart(TimeOfDay doNotDisturbStart) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return await sharedPreferences.setString(
      _doNotDisturbStartKey,
      getHourTextFromTime(doNotDisturbStart));
  }

  static Future<bool> updateDoNotDisturbEnd(TimeOfDay doNotDisturbEnd) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return await sharedPreferences.setString(
      _doNotDisturbEndKey,
      getHourTextFromTime(doNotDisturbEnd));
  }

  static Future<bool> toggleDisabled() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool currentSetting = sharedPreferences.getBool(_disabledKey) ?? _defaultIsDisabled;

    return await sharedPreferences.setBool(_disabledKey, !currentSetting);
  }

  bool get isSilent {
    if (!doNotDisturb) { return false; }

    TimeOfDay currentTime = TimeOfDay.fromDateTime(DateTime.now());
    int minuteOfTheDay = getMinutesInADay(currentTime);

    int startMinutes = getMinutesInADay(doNotDisturbStart);
    int endMinutes = getMinutesInADay(doNotDisturbEnd);
    bool isDaytimeDoNotDisturb = startMinutes < endMinutes;

    bool isInTimeBracket = isDaytimeDoNotDisturb
      ? minuteOfTheDay > startMinutes && minuteOfTheDay < endMinutes
      : !(minuteOfTheDay < startMinutes && minuteOfTheDay > endMinutes);

    return isInTimeBracket;
  }
}
