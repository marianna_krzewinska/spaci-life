import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:spaci/utils.dart';
import 'package:spaci/main_state.dart';
import 'package:spaci/settings/settings_state.dart';
import 'package:spaci/settings/settings_model.dart';


class SettingsView extends StatefulWidget {
  static const route = '/settings';

  @override
  State<StatefulWidget> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  final _formKey = GlobalKey<FormState>();

  bool _silentTime;

  TimeOfDay _startTime;
  TimeOfDay _endTime;

  @override
  Widget build(BuildContext context) {

    final store = StoreProvider.of<AppState>(context);
    store.dispatch(GetSettingsAction());

    return StoreConnector<AppState, Settings>(
      converter: (store) => store.state.settings,
      distinct: true,
      builder: (context, Settings settings) {
        _startTime = settings.doNotDisturbStart;
        _endTime = settings.doNotDisturbEnd;
        _silentTime = settings.doNotDisturb;

        return Scaffold(
          appBar: AppBar(
            title: Text('Settings'),
          ),

          body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                    title: Text('Silent time'),
                    trailing: Switch(
                      value: _silentTime,
                      onChanged: (value) {
                        store.dispatch(SetSettingsDoNotDisturbAction(value));
                      }
                    ),
                  ),

                  ListTile(
                    title: Text("Silent time start: ${getHourTextFromTime(_startTime)}"),
                    trailing: Icon(Icons.keyboard_arrow_down),
                    enabled: _silentTime,
                    onTap: () async {
                      TimeOfDay selectedTime = await showTimePicker(
                        context: context,
                        initialTime: _startTime,
                      ) ?? _startTime;

                      store.dispatch(SetSettingsDoNotDisturbStartAction(selectedTime));
                    },
                  ),

                  ListTile(
                    title: Text("Silent time end: ${getHourTextFromTime(_endTime)}"),
                    trailing: Icon(Icons.keyboard_arrow_down),
                    enabled: _silentTime,
                    onTap: () async {
                      TimeOfDay selectedTime = await showTimePicker(
                        context: context,
                        initialTime: _endTime,
                      ) ?? _endTime;

                      store.dispatch(SetSettingsDoNotDisturbEndAction(selectedTime));
                    },
                  ),
                ]
              )
            )
          ),
        );
      }
    );
  }
}
