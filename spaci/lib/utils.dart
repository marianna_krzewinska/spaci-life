import 'package:flutter/material.dart';

int getMinutesInADay(TimeOfDay time) => time.hour * 60 + time.minute;

String getHourTextFromMinutes(int minutes) {
  String hour = ((minutes / 60) % 24).floor().toString().padLeft(2, '0');
  String minute = (minutes % 60).toString().padLeft(2, '0');

  return '$hour:$minute';
}

String getHourTextFromTime(TimeOfDay time) =>
  '${time?.hour.toString().padLeft(2, '0')}:${time?.minute.toString().padLeft(2, '0')}';

String dateToString(DateTime date) =>
  '${date.year}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}';
