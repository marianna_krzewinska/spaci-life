import 'dart:async';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';
import 'package:foreground_service/foreground_service.dart';

import 'package:spaci/logs.dart';
import 'package:spaci/utils.dart';
import 'package:spaci/main_database.dart';
import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/settings/settings_model.dart';

const _intervalLag = 1;

const _initialIntervalSeconds = 60 - _intervalLag;
const _interval15min = 60 * 15 - _intervalLag;

Future<void> updateForegroundService() async {
  bool serviceAlreadyStarted = await ForegroundService.foregroundServiceIsStarted();
  bool isDisabled = await _isDisabled();
  bool noSpacers = await database.getSpacersCount() == 0;

  bool shouldStopService = serviceAlreadyStarted && (isDisabled || noSpacers);

  if (shouldStopService) {
    return ForegroundService.stopForegroundService();
  }

  bool shouldStartService = !serviceAlreadyStarted && !isDisabled && !noSpacers;

  if (shouldStartService) {
    return _startForegroundService();
  }

  if (serviceAlreadyStarted && !isDisabled && !noSpacers) {
    return _restartNotification();
  }

  return;
}

Future<void> _startForegroundService() async {
  await ForegroundService.setServiceIntervalSeconds(_initialIntervalSeconds);

  await _restartNotification();

  await ForegroundService.startForegroundService(_callback);
  await ForegroundService.getWakeLock();
}

Future<void> _callback() async {
  WidgetsFlutterBinding.ensureInitialized();

  // purposefuly asynchronous, so they don't block alerting
  logAlertExecution();
  _runEvery15min();

  Settings settings = await Settings.getFromStore();

  if (settings.isSilent) {
    return;
  }

  return await _runSpacers();
}

Future<void> _runEvery15min() async {
  int currentInterval = await ForegroundService.getServiceIntervalSeconds();
  bool isQuarter = DateTime.now().minute % 15 == 0;

  if (currentInterval == _initialIntervalSeconds && isQuarter) {
    await ForegroundService.setServiceIntervalSeconds(_interval15min);
  }

  return;
}

Future _restartNotification() => database.getSpacers()
  .then((List<AppSpacer> spacers) {
    TimeOfDay currentTime = TimeOfDay.fromDateTime(DateTime.now());
    int minuteOfTheDay = getMinutesInADay(currentTime);

    return _updateNotificationText(spacers, minuteOfTheDay);
  });

void playSound(String sound) {
  debugPrint('play sound: $sound');

  AudioCache(respectSilence: false)
    .play(
      'sounds/$sound.wav',
      isNotification: false,
      stayAwake: true,
      volume: sounds[sound].volume,
    );
}

bool _shouldRun(int minuteOfTheDay, String intervalKey, int currentMinute) {
  int intervalMinutes = intervals[intervalKey].intervalInMinutes;

  return intervalMinutes < 60
    ? (currentMinute % intervalMinutes == 0)
    : (minuteOfTheDay % intervalMinutes == 0);
}

Future<void> _runSpacers() => database.getSpacers()
  .then((List<AppSpacer> spacers) async {
    TimeOfDay currentTime = TimeOfDay.fromDateTime(DateTime.now());
    int minuteOfTheDay = getMinutesInADay(currentTime);

    List<AppSpacer> spacersToPlay = spacers
      .where((spacer) => _shouldRun(
        minuteOfTheDay,
        spacer.intervalKey,
        currentTime.minute))
      .toList();

    if (spacersToPlay.isEmpty) {
      return;
    }

    for (var i = 0; i < spacersToPlay.length; i++) {
      Timer(Duration(seconds: 2 * i), () => playSound(spacersToPlay.elementAt(i).sound));
    }

    await _updateNotificationText(spacers, minuteOfTheDay);

    return;
  });

Future<bool> _isDisabled() async {
  Settings settings = await Settings.getFromStore();

  return settings.isDisabled;
}

String _getSpacerNextRunTime(AppSpacer spacer, int minutesOfTheDay) {
  int interval = intervals[spacer.intervalKey].intervalInMinutes;

  int nextTimeInMinutes = (minutesOfTheDay / interval).floor() * interval + interval;

  return getHourTextFromMinutes(nextTimeInMinutes);
}

Future<void> _updateNotificationText(List<AppSpacer> spacers, int minutesOfTheDay) async {
  await ForegroundService.notification.startEditMode();

  List<SpacerMessage> spacersMessages = spacers
    .map((spacer) => SpacerMessage(
      _getSpacerNextRunTime(spacer, minutesOfTheDay),
      spacer.name))
    .toList();

  spacersMessages.sort((a, b) => a.time.compareTo(b.time));

  SpacerMessage closestSpacer = spacersMessages.first;

  List<String> names = spacersMessages
    .where((element) => element.time == closestSpacer.time)
    .map((element) => element.name)
    .toList();

  bool isMany = names.length > 1;

  await ForegroundService.notification
      .setTitle('Next Spacer${isMany ? 's' : ''} at: ${closestSpacer.time}');
  await ForegroundService.notification
      .setText(names.join(', '));

  await ForegroundService.notification.finishEditMode();

  return;
}

class SpacerMessage {
  String name;
  String time;

  SpacerMessage(this.time, this.name);
}
