import 'dart:async';
import 'package:redux_epics/redux_epics.dart';

import 'package:spaci/foreground_service.dart';
import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/spacers/spacers_state.dart';
import 'package:spaci/settings/settings_model.dart';
import 'package:spaci/settings/settings_state.dart';


class AppState {
  final List<AppSpacer> spacers;
  final String title = 'Spacers: ';
  final Settings settings;

  AppState(this.spacers, this.settings);

  factory AppState.initial() => AppState(initialSpacersState, settingsInitialState);
}

final epics = combineEpics<AppState>([
  updateForegroundServiceEpic,
  ...settingsEpics,
  ...spacersEpics,
]);

AppState reducer(AppState state, action) {
  if (action is SpacerAction) {
    return AppState(
      spacersReducer(state, action),
      state.settings,
    );
  }

  if (action is SettingsAction) {
    return AppState(
      state.spacers,
      settingsReducer(state, action),
    );
  }

  return state;
}

Stream<dynamic> updateForegroundServiceEpic(Stream<dynamic> actions, EpicStore<AppState> _) {
  return actions
    .where((action) => [
      SpacerActions.DeleteSpacerSuccess,
      SpacerActions.UpdateSpacerSuccess,
      SpacerActions.CreateSpacerSuccess,
      SettingsActions.GetSettingsSuccess,
    ].contains(action.type))
    .asyncMap((action) async {
      await updateForegroundService();

      return EmptyAction();
    });
}

class EmptyAction {
  final MainActions type = MainActions.Empty;

  EmptyAction();
}

class SetupForegroundServiceAction {
  final MainActions type = MainActions.SetupForegroundService;

  SetupForegroundServiceAction();
}

enum MainActions {
  Empty,
  SetupForegroundService,
}
