import 'dart:io';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import 'package:spaci/utils.dart';

Future<File> logAlertExecution() async => File(await _getLogFilePath())
  .writeAsString('\r\n${DateTime.now().toString()}', mode: FileMode.append);

Future<bool> scheduleLogsReset() async {
  await AndroidAlarmManager.initialize();

  DateTime now = DateTime.now();

  return AndroidAlarmManager.periodic(
    Duration(days: 2),
    1,
    _resetLogs,
    rescheduleOnReboot: true,
    exact: true,
    wakeup: true,
    startAt: DateTime(now.year, now.month, now.day).add(Duration(days: 1)),
  );
}

Future<String> _getLogFilePath() async {
  final directory = await getApplicationDocumentsDirectory();

  return '${directory.path}/logs.txt';
}

Future _resetLogs() {
  return _getLogFilePath()
    .then((filepath) => File(filepath))
    .then((file) =>  file.readAsString()
      .then((contents) => contents
        .split('\r\n')
        .where((line) {
          DateTime now = DateTime.now();
          DateTime yesterday = now.subtract(Duration(days: 1));
          return line.startsWith(dateToString(now)) || line.startsWith(dateToString(yesterday));
        })
        .join('\r\n'))
      .then((filtered) => file.writeAsString(filtered)));
}

class LogsView extends StatefulWidget {
  static const route = '/logs';

  @override
  State<StatefulWidget> createState() => _LogsViewState();
}

class _LogsViewState extends State<LogsView> {
  String _logs = '';

  @override
  Widget build(BuildContext context) {
    _getLogFilePath()
      .then((filepath) => File(filepath))
      .then((file) =>  file.readAsString())
      .then((String contents) {
        setState(() {
          _logs = contents;
        });
      });

    return Scaffold(
      appBar: AppBar(
        title: Text('Logs'),
      ),

      body: SingleChildScrollView(child: Text('$_logs'))
    );
  }
}
