import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../utils.dart';


Future goToSettings(WidgetTester tester) async {
  final goToSettings = find.byIcon(Icons.settings);

  expect(goToSettings, findsOneWidget, reason: 'didn\'t find one settings icon');

  await tester.tap(goToSettings);

  await tester.pumpAndSettle(Duration(seconds: 2));
}

void main() {
  group('"SettingsView"', () {
    testWidgets('gear wheel icon tap should navigate to Settings view',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeApp(tester);
        await goToSettings(tester);

        final settingsTitle = find.text('Settings');

        expect(settingsTitle, findsOneWidget, reason: 'incorrect title');
      });
    });

    group('basic state', () {
      testWidgets('should show silent time toggle', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(tester);
          await goToSettings(tester);

          final silentTimeLabel = find.text('Silent time');
          final silentTimeToggle = find.byType(Switch);

          expect(silentTimeLabel, findsOneWidget, reason: 'incorrect silent time label');
          expect(silentTimeToggle, findsOneWidget, reason: 'didn\'t find toggle');
        });
      });

      testWidgets('should show start time disabled', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(tester);
          await goToSettings(tester);

          final startTimeLabel = find.text('Silent time start: 22:00');
          final startTimeExpandIcon = find.byIcon(Icons.keyboard_arrow_down).at(0);

          expect(startTimeLabel, findsWidgets, reason: 'incorrect start time label');
          expect(startTimeExpandIcon, findsWidgets, reason: 'didn\'t find expand icon');
        });
      });

      testWidgets('should show end time disabled', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(tester);
          await goToSettings(tester);

          final startTimeLabel = find.text('Silent time end: 10:00');
          final startTimeExpandIcon = find.byIcon(Icons.keyboard_arrow_down).at(1);

          expect(startTimeLabel, findsWidgets, reason: 'incorrect start time label');
          expect(startTimeExpandIcon, findsWidgets, reason: 'didn\'t find expand icon');
        });
      });
    });
  });
}
