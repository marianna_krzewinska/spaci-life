import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/settings/settings_model.dart';
import 'package:spaci/settings/settings_state.dart';

void main() {
  group('"settingsInitialState"', () {
    test('should contain all settings fields', () {
      expect(settingsInitialState.doNotDisturb, false,
        reason: 'do not disturb is set to true');
      expect(settingsInitialState.doNotDisturbEnd is TimeOfDay, true,
        reason: 'end time is not TimeOfDay');
      expect(settingsInitialState.doNotDisturbStart is TimeOfDay, true,
        reason: 'start time is not TimeOfDay');
      expect(settingsInitialState.isDisabled, true, reason: 'is disabled flag is not "true"');
    });
  });

  group('"GetSettingsAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      Settings settingsState = settingsReducer(appState, GetSettingsAction());

      expect(settingsState == appState.settings, true, reason: 'state has changed');
    });
  });

  group('"GetSettingsActionSuccess"', () {

    test('should overwrite state', () {
      AppState appState = AppState.initial();

      Settings settings = Settings();

      Settings settingsState = settingsReducer(appState, GetSettingsSuccessAction(settings));

      expect(settingsState == appState.settings, false, reason: 'state didn\'t change');
      expect(settingsState == settings, true, reason: 'state is not equal to passed settings');
    });
  });

  group('"SetSettingsDoNotDisturbAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      Settings settingsState = settingsReducer(appState, SetSettingsDoNotDisturbAction(true));

      expect(settingsState == appState.settings, true, reason: 'state has changed');
    });
  });

  group('"SetSettingsDoNotDisturbStartAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      Settings settingsState = settingsReducer(appState,
        SetSettingsDoNotDisturbStartAction(TimeOfDay(hour: 1, minute: 2)));

      expect(settingsState == appState.settings, true, reason: 'state has changed');
    });
  });

  group('"SetSettingsDoNotDisturbEndAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      Settings settingsState = settingsReducer(appState,
        SetSettingsDoNotDisturbEndAction(TimeOfDay(hour: 1, minute: 2)));

      expect(settingsState == appState.settings, true, reason: 'state has changed');
    });
  });

  group('"ToggleSettingsDisabledAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      Settings settingsState = settingsReducer(appState, ToggleSettingsDisabledAction());

      expect(settingsState == appState.settings, true, reason: 'state has changed');
    });
  });
}
