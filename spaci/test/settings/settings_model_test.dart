import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/settings/settings_model.dart';
import 'package:spaci/utils.dart';

void main() {
  group('Settings model', () {
    test('should initialize with default values if none provided', () {
      Settings settings = Settings();

      expect(settings.doNotDisturb, false,
        reason: 'do not disturb is set to true');
      expect(settings.doNotDisturbEnd is TimeOfDay, true,
        reason: 'end time is not TimeOfDay');
      expect(settings.doNotDisturbStart is TimeOfDay, true,
        reason: 'start time is not TimeOfDay');
      expect(settings.isDisabled, true, reason: 'is disabled flag is not "true"');
    });

    test('should initialize with provided values', () {
      TimeOfDay endTime = TimeOfDay(hour: 9, minute: 0);
      TimeOfDay startTime = TimeOfDay(hour: 23, minute: 0);
      Settings settings = Settings(
        doNotDisturb: true,
        doNotDisturbEnd: endTime,
        doNotDisturbStart: startTime);

      expect(settings.doNotDisturb, true,
        reason: 'do not disturb is not as provided');
      expect(settings.doNotDisturbEnd == endTime, true,
        reason: 'end time is not as provided');
      expect(settings.doNotDisturbStart == startTime, true,
        reason: 'start time is not as provided');
      expect(settings.isDisabled, true,
        reason: 'is disabled flag is not as provided');
    });

    group('"clone"', () {
      test('should clone a Settings object', () {
        Settings originalSettings = Settings();
        Settings clone = Settings.clone(originalSettings);

        expect(originalSettings == clone, false, reason: 'clone is not a new object');
        expect(originalSettings.doNotDisturb == clone.doNotDisturb, true,
          reason: 'do not disturb doesn\'t match');
        expect(originalSettings.doNotDisturbEnd == clone.doNotDisturbEnd, true,
          reason: 'end time doesn\'t match');
        expect(originalSettings.doNotDisturbStart == clone.doNotDisturbStart, true,
          reason: 'start time doesn\'t match');
      });
    });

    group('"timeOfDayFromString"', () {
      test('should create TimeOfDay object from "dd:dd" string', () {
        TimeOfDay defaultTime = TimeOfDay(hour: 10, minute: 32);
        String timeText = '11:17';
        TimeOfDay time = Settings.timeOfDayFromString(timeText, defaultTime);

        String parsedTimeToText = getHourTextFromTime(time);

        expect(parsedTimeToText == timeText, true,
          reason: 'parsed and original time don\'t match ');
      });

      test('should create return default time if format is incorrect', () {
        TimeOfDay defaultTime = TimeOfDay(hour: 10, minute: 32);
        String timeText = '1117';
        TimeOfDay time = Settings.timeOfDayFromString(timeText, defaultTime);

        String parsedTimeToText = getHourTextFromTime(time);

        expect(parsedTimeToText == timeText, false,
          reason: 'accepted incorrect time format');
        expect(parsedTimeToText == getHourTextFromTime(defaultTime), true,
          reason: 'default time was not returned');
      });
    });

    group('"isSilent"', () {
      test('should be "false" if do not disturb flag is set to "false"', () {
        Settings settings = Settings();

        expect(settings.doNotDisturb, false, reason: 'do not disturb default is "true"');
        expect(settings.isSilent, false, reason: 'is silent is truthful');
      });

      test('should be "false" if before the start and end time window', () {
        Settings settings = Settings(
          doNotDisturb: true,
          doNotDisturbEnd: TimeOfDay.fromDateTime(DateTime.now().add(Duration(minutes: 10))),
          doNotDisturbStart: TimeOfDay.fromDateTime(DateTime.now().add(Duration(minutes: 5))),
        );

        expect(settings.isSilent, false, reason: 'is silent is truthful');
      });

      test('should be "false" if after the start and end time window', () {
        Settings settings = Settings(
          doNotDisturb: true,
          doNotDisturbEnd: TimeOfDay.fromDateTime(DateTime.now().subtract(Duration(minutes: 5))),
          doNotDisturbStart: TimeOfDay.fromDateTime(DateTime.now().subtract(Duration(minutes: 10))),
        );

        expect(settings.isSilent, false, reason: 'is silent is truthful');
      });

      test('should be "true" if within the start and end time window', () {
        Settings settings = Settings(
          doNotDisturb: true,
          doNotDisturbEnd: TimeOfDay
            .fromDateTime(DateTime.now().add(Duration(minutes: 5))),
          doNotDisturbStart: TimeOfDay
            .fromDateTime(DateTime.now().subtract(Duration(minutes: 5))),
        );

        expect(settings.isSilent, true, reason: 'is silent is false');
      });
    });
  });
}
