import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/utils.dart';

void main() {
  group('"getMinutesInADay"', () {
    test('should calculate overall number of minutes in a TimeOfDay', () {
      TimeOfDay time = TimeOfDay(hour: 1, minute: 13);
      int minutesCount = 73;

      int minutesFromFunction = getMinutesInADay(time);

      expect(minutesFromFunction == minutesCount, true,
        reason: 'calculated minutes are incorrect');
    });
  });

  group('"getHourTextFromTime"', () {
    test('should parse TimeOfDay to text in "HH:mm" format', () {
      TimeOfDay time = TimeOfDay(hour: 1, minute: 14);
      String expectedText = '01:14';
      String actualText = getHourTextFromTime(time);

      expect(actualText, expectedText, reason: 'time doesn\'t match expected');
    });
  });

  group('"getHourTextFromMinutes"', () {
    test('should parse minutes count to text in "HH:mm" format', () {
      int minutes = getMinutesInADay(TimeOfDay(hour: 1, minute: 14));
      String expectedText = '01:14';
      String actualText = getHourTextFromMinutes(minutes);

      expect(actualText, expectedText, reason: 'time doesn\'t match expected');
    });
  });

  group('"dateToString"', () {
    test('should parse date to string in format YYYY-MM-DD', () {
      DateTime dateTime = DateTime(2021, 4, 15);
      String expectedText = '2021-04-15';
      String actualText = dateToString(dateTime);

      expect(actualText, expectedText, reason: 'date text is incorrect');
    });
  });
}
