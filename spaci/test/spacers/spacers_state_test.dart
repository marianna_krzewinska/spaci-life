import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/spacers/spacer_model.dart';
import 'package:spaci/spacers/spacers_state.dart';
import 'package:spaci/main_state.dart';

void main() {
  group('"initialState"', () {
    test('should be an empty list', () {
      expect(initialSpacersState is List, true, reason: 'is not a list');
      expect(initialSpacersState.isEmpty, true, reason: 'is not empty');
    });
  });

  group('"UpdateSpacerAction"', () {

    test('should not change state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, UpdateSpacerAction(spacer.id));

      expect(spacersState == appState.spacers, true, reason: 'state is not changed');
    });
  });

  group('"UpdateSpacerSuccessAction"', () {

    test('should return new list as state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, UpdateSpacerSuccessAction(spacer));

      expect(spacersState == appState.spacers, false, reason: 'no change in state');
      expect(spacersState is List, true, reason: 'state is not a list');
      expect(spacersState.isEmpty, true, reason: 'state is not empty');
    });
  });

  group('"CreateSpacerAction"', () {

    test('should not change state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, CreateSpacerAction(spacer));

      expect(spacersState == appState.spacers, true, reason: 'state has changed');
    });
  });

  group('"CreateSpacerSuccessAction"', () {

    test('should add Spacer object to the state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, CreateSpacerSuccessAction(spacer));

      expect(spacersState == appState.spacers, false, reason: 'no change in state');
      expect(spacersState.isNotEmpty, true, reason: 'Spacers list is empty');
      expect(spacersState.first == spacer, true, reason: 'Spacer was not addede to the list');
    });
  });

  group('"DeleteSpacerAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, DeleteSpacerAction(spacer.id));

      expect(spacersState == appState.spacers, true, reason: 'state has changed');
    });
  });

  group('"DeleteSpacerSuccessAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, DeleteSpacerSuccessAction(spacer.id));

      expect(spacersState == appState.spacers, false, reason: 'state has not changed');
      expect(spacersState is List, true, reason: 'state is not a list');
      expect(spacersState.isEmpty, true, reason: 'state is not empty');
    });
  });

  group('"GetSpacersAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();

      List<AppSpacer> spacersState = spacersReducer(appState, GetSpacersAction());

      expect(spacersState == appState.spacers, true, reason: 'state has changed');
    });
  });

  group('"GetSpacersSuccessAction"', () {
    test('should not change state', () {
      AppState appState = AppState.initial();
      AppSpacer spacer = AppSpacer.create();

      List<AppSpacer> spacersState = spacersReducer(appState, GetSpacersSuccessAction([spacer]));

      expect(spacersState == appState.spacers, false, reason: 'state has not changed');
      expect(spacersState is List, true, reason: 'Spacers list is not a list');
      expect(spacersState.isNotEmpty, true, reason: 'Spacers list is empty');
      expect(spacersState.first == spacer, true, reason: 'Spacers were not added to the list');
    });
  });
}
