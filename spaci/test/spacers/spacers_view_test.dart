import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/settings/settings_state.dart';
import 'package:spaci/spacers/spacer_model.dart';
import '../utils.dart';

void main() {
  group('"SpacersView"', () {
    testWidgets('should show number of spacers in title',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeApp(
          tester,
          initialState: AppState([AppSpacer.create()], settingsInitialState));

        final spacersTitle = find.text('Spacers: 1');

        expect(spacersTitle, findsOneWidget, reason: 'incorrect title');
      });
    });

    testWidgets('should show list with Spacers summaries',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeApp(
          tester,
          initialState: AppState(
            [AppSpacer.create().update(newName: 'pomodoro')],
            settingsInitialState));

        final spacerSummary = find.text('pomodoro, interval: 15 min');

        expect(spacerSummary, findsOneWidget, reason: 'incorrect summary');
      });
    });
  });
}
