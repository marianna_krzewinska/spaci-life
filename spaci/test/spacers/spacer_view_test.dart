import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/main_state.dart';
import 'package:spaci/settings/settings_state.dart';
import 'package:spaci/spacers/spacer_model.dart';
import '../utils.dart';

Future<void> goToSpacerView(WidgetTester tester, String spacerName) async {
  final spacerSummary = find.text('$spacerName, interval: 15 min');

  await tester.tap(spacerSummary);

  await tester.pumpAndSettle(Duration(seconds: 2));
}

void main() {
  group('"SpacerView"', () {
    testWidgets('click on a spacer should redirect to edit view',
      (WidgetTester tester) async {
      await tester.runAsync(() async {
        await initializeApp(
          tester,
          initialState: AppState(
            [AppSpacer.create().update(newName: 'pomodoro')],
            settingsInitialState));

        await goToSpacerView(tester, 'pomodoro');

        final nameLabel = find.text('Name: ');

        expect(nameLabel, findsOneWidget, reason: 'name field not found');
      });
    });

    group('basic state', () {
      testWidgets('should show a name field', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final nameLabel = find.text('Name: ');
          final textField = find.byType(TextFormField);

          expect(nameLabel, findsOneWidget, reason: 'name label not found');
          expect(textField, findsOneWidget, reason: 'name text field not found');
        });
      });

      testWidgets('should show an interval dropdown', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final intervalLabel = find.text('Interval: ');
          final intervalDropdown = find.text('15 min');

          expect(intervalLabel, findsOneWidget, reason: 'interval label not found');
          expect(intervalDropdown, findsOneWidget, reason: 'interval dropdown not found');
        });
      });

      testWidgets('should show a sound dropdown and play button',
        (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final soundLabel = find.text('Sound: ');
          final soundDropdown = find.text('Single');
          final soundPlayBtn = find.byIcon(Icons.play_arrow);

          expect(soundLabel, findsOneWidget, reason: 'sound label not found');
          expect(soundDropdown, findsOneWidget, reason: 'sound dropdown not found');
          expect(soundPlayBtn, findsOneWidget, reason: 'sound play button not found');
        });
      });

      testWidgets('should show a delete button',
        (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final deleteText = find.text('Delete');
          final deleteIcon = find.byIcon(Icons.delete);

          expect(deleteText, findsOneWidget, reason: 'delete text not found');
          expect(deleteIcon, findsOneWidget, reason: 'delete icon not found');
        });
      });
    });

    group('basic interactions', () {
      testWidgets('interval dropdown should show all available intervals',
        (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final intervalDropdown = find.text('15 min');

          await tester.tap(intervalDropdown);

          final intervalOptions = intervals.keys
            .map((key) => intervals[key].name)
            .map((name) => {
              'name': name,
              'widget': find.text(name),
            })
            .toList();

          intervalOptions.forEach((option) {
            expect(
              option['widget'],
              findsOneWidget,
              reason: 'option "${option['name']}" not found');
          });
        });
      });

      testWidgets('sound dropdown should show all available sounds',
        (WidgetTester tester) async {
        await tester.runAsync(() async {
          await initializeApp(
            tester,
            initialState: AppState(
              [AppSpacer.create().update(newName: 'pomodoro')],
              settingsInitialState));

          await goToSpacerView(tester, 'pomodoro');

          final soundDropdown = find.text('Single');

          await tester.tap(soundDropdown);

          final soundOptions = sounds.keys
            .map((key) => sounds[key].name)
            .map((name) => {
              'name': name,
              'widget': find.text(name),
            })
            .toList();

          soundOptions.forEach((option) {
            expect(
              option['widget'],
              findsOneWidget,
              reason: 'option "${option['name']}" not found');
          });
        });
      });
    });
  });
}
