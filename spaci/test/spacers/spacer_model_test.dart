import 'package:flutter_test/flutter_test.dart';

import 'package:spaci/spacers/spacer_model.dart';


int _id = 0;
String _name = 'some name';
String _intervalKey = intervals['Min30'].key;
String _soundKey = sounds['double'].key;


AppSpacer _initializeSpacer() {
  return AppSpacer(_id, _name, _intervalKey, _soundKey);
}


void main() {

  group('AppSpacer model', () {
    test('should be initialized with provided values', () {
      AppSpacer spacer = _initializeSpacer();
      expect(spacer is AppSpacer, true, reason: 'is not an instance of AppSpacer');
      expect(spacer.id, _id, reason: 'id doesn\'t match');
      expect(spacer.name, _name, reason: 'name doesn\'t match');
      expect(spacer.intervalKey, _intervalKey, reason: 'intervalKey doesn\'t match');
      expect(spacer.sound, _soundKey, reason: 'sound doesn\'t match');
    });

    test('should be initialized with default values if provided are incorrect', () {
      int id = 1;
      String name = 'test name';

      AppSpacer spacer = AppSpacer(id, name, 'abc', 'def');
      expect(spacer is AppSpacer, true, reason: 'is not an instance of AppSpacer');
      expect(spacer.id, id, reason: 'id doesn\'t match');
      expect(spacer.name, name, reason: 'name doesn\'t match');
      expect(spacer.intervalKey, intervals['Min15'].key, reason: 'default interval mismatch');
      expect(spacer.sound, sounds['single'].key, reason: 'default sound mismatch');
    });

    group('"create"', () {
      test('should be a static function', () {
        expect(AppSpacer.create is Function, true, reason: 'is not a function');
      });

      test('should create a default Spacer', () {
        AppSpacer spacer = AppSpacer.create();
        expect(spacer.id is int, true, reason: 'id is not am int');
        expect(spacer.name, '', reason: 'name is not an empty string');
        expect(spacer.intervalKey, intervals['Min15'].key, reason: 'default interval mismatch');
        expect(spacer.sound, sounds['single'].key, reason: 'default sound mismatch');
      });
    });

    group('"clone"', () {
      test('should be a static function', () {
        expect(AppSpacer.clone is Function, true, reason: 'is not a function');
      });

      test('should create a copy of an existing spacer', () {
        AppSpacer originalSpacer = AppSpacer.create();
        AppSpacer clone = AppSpacer.clone(originalSpacer);

        expect(originalSpacer == clone, false, reason: 'didn\'t create a new instance of a Spacer');
        expect(originalSpacer.id == clone.id, true, reason: 'id mismatch');
        expect(originalSpacer.name == clone.name, true, reason: 'name mismatch');
        expect(originalSpacer.intervalKey == clone.intervalKey, true, reason: 'interval mismatch');
        expect(originalSpacer.sound == clone.sound, true, reason: 'sound mismatch');
      });

    });

    group('"update"', () {
      test('should return a new Spacer with changed name, intervalKey and sound but not id', () {
        AppSpacer initialSpacer = _initializeSpacer();
        AppSpacer updated = initialSpacer.update(
          newName: 'changed',
          newIntervalKey: 'changed',
          newSound: 'changed');

        expect(initialSpacer == updated, false, reason: 'didn\'t create a new instance of a Spacer');
        expect(initialSpacer.id == updated.id, true, reason: 'id mismatch');
        expect(initialSpacer.name == updated.name, false, reason: 'name didn\'t change');
        expect(initialSpacer.intervalKey == updated.intervalKey, false, reason: 'interval didn\'t change');
        expect(initialSpacer.sound == updated.name, false, reason: 'sound didn\'t change');
      });
    });

    group('"toMap"', () {
      test('should return a map with fields "id", "name", "intervalKey" and "sound"', () {
        AppSpacer spacer = _initializeSpacer();
        dynamic spacerMap = spacer.toMap();

        expect(spacer.id == spacerMap['id'], true, reason: 'id mismatch');
        expect(spacer.name == spacerMap['name'], true, reason: 'name mismatch');
        expect(spacer.intervalKey == spacerMap['intervalKey'], true, reason: 'interval mismatch');
        expect(spacer.sound == spacerMap['sound'], true, reason: 'sound mismatch');
      });
    });
  });

}
