import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:spaci/main.dart';
import 'package:spaci/main_state.dart';

Future initializeApp(WidgetTester tester, { AppState initialState }) async {
  final epicMiddleware = EpicMiddleware<AppState>(epics);

  final store = Store<AppState>(
    reducer,
    initialState: initialState ?? AppState.initial(),
    middleware: [
      epicMiddleware,
      NavigationMiddleware<AppState>(),
    ]
  );

  await tester.pumpWidget(StoreProvider<AppState>(
    store: store,
    child: App(store: store)),
    Duration(seconds: 1000),
  );
}
